import pytest
from key import key


@pytest.fixture
def minimum_default_search_payload():
    """
    Набор стандартных минимальных тестовых данных
    """
    return {
        "key": key,
        "location": "-33.8670522,151.1957362",
        "radius": 500}


@pytest.fixture
def maximum_default_search_payload():
    """
    Набор минимальных тестовых данных
    с указанием радиуса поиска
    """
    return {
        "key": key,
        "location": "-33.8670522,151.1957362",
        "radius": 1500,
        "type": "restaurant",
        "keyword": "cruise"}


@pytest.fixture
def rankby_distance_search_payload():
    """
    Набор стандартных минимальных тестовых данных
    с сортировкой по удаленности
    """
    return {
        "key": key,
        "location": "-33.8670522,151.1957362",
        "rankby": "distance",
        "type": "restaurant",
        "keyword": "cruise"}


@pytest.fixture
def search_payload_with_rankby_and_radius():
    """
    Набор тестовых данных
    с указанием радиуса и сортировкой по удаленности
    """
    return {
        "key": key,
        "location": "-33.8670522,151.1957362",
        "rankby": "distance",
        "radius": 1500,
        "type": "restaurant",
        "keyword": "cruise"}


@pytest.fixture(params=[0, -50, 'afv'])
def search_payload_wrong_key(maximum_default_search_payload, request):
    """
    Набор тестовых данных для теста с ошибочным ключом
    """
    data = maximum_default_search_payload.copy()
    data['key'] = request.param
    return data


@pytest.fixture(params=[0, -50, 'afv', '', 50005])
def search_payload_wrong_radius(maximum_default_search_payload, request):
    """
    Набор тестовых данных с ошибочным радиусом
    """
    data = maximum_default_search_payload.copy()
    data['radius'] = request.param
    return data
