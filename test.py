# coding: utf-8
import pytest
import requests
search_url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"


"""
Набор тесткейсов для проверки остаточных знаний по тестированию backend
"""


def check_invalid_request(resp):
    """
    проверка ответа на не правильный запрос
    функция принимает на вход ответ от сервиса
    проверят ответ на соответсвие по статусу и коду
    """
    search_json = resp.json()
    status = search_json["status"]
    assert status == 'INVALID_REQUEST'
    assert resp.status_code == 200


def check_request_denied(resp):
    """
    проверка ответа на запрос с не верным ключом
    функция принимает на вход ответ от сервиса
    проверят ответ на соответсвие по статусу, коду
    и сообщению об ошибке
    """
    search_json = resp.json()
    status = search_json["status"]
    error_message = search_json["error_message"]
    assert status == 'REQUEST_DENIED'
    assert error_message == 'The provided API key is invalid.'
    assert resp.status_code == 200


def test_minimum_validate_request(minimum_default_search_payload):
    """
    тест
    запрос с минимальным набором корректных данных
    """
    resp = requests.get(search_url, minimum_default_search_payload)
    search_json = resp.json()

    place_id = search_json["results"][0]["place_id"]
    assert place_id != 0
    assert resp.status_code == 200


def test_maximum_validate_request(maximum_default_search_payload):
    """
    тест
    запрос с максимальным набором корректных данных
    """
    resp = requests.get(search_url, maximum_default_search_payload)
    search_json = resp.json()

    types = search_json["results"][0]["types"][1]
    assert types == 'restaurant'
    assert resp.status_code == 200


def test_validate_request_with_rankby_distance(rankby_distance_search_payload):
    """
    тест
    запрос с сортировкой по удаленности набором корректных данных
    """
    resp = requests.get(search_url, rankby_distance_search_payload)
    search_json = resp.json()

    types = search_json["results"][0]["types"][1]
    assert types == 'restaurant'
    assert resp.status_code == 200


def test_with_wrong_key(search_payload_wrong_key):
    """
    тест
    негативный запрос с не корректным ключом
    """
    resp = requests.get(search_url, search_payload_wrong_key)
    check_request_denied(resp)


def test_with_wrong_radius(search_payload_wrong_radius):
    """
    тест
    негативный запрос с не корректным значением радиуса
    """
    resp = requests.get(search_url, search_payload_wrong_radius)
    check_invalid_request(resp)


def test_with_rankby_radius(search_payload_with_rankby_and_radius):
    """
    тест
    негативный запрос с указанием радиуса и типа сортировки
    """
    resp = requests.get(search_url, search_payload_with_rankby_and_radius)
    check_invalid_request(resp)
